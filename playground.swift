import Foundation

let name:String = "Мансуров Владимир Александрович" 
let dateOfBirth:String = "15.04.2002"
let age:Int = 21

let education = "27.03.03 - Системный анализ и управление"
let groupName = "0395"
let course = 4

print ("Меня зовут " + name + ".")
print ("Я родился " + dateOfBirth + " и на данный момент мне " + String(age) + " год.")
print ("Обучаюсь на направлении " + education + ".")
print ("Уже целых " + String(course) + " курса нахожусь в группе " + groupName + ".")